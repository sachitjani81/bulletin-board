from flask.views import MethodView
from flask import render_template, redirect, make_response, url_for, request
from models import *
import urllib.parse, hashlib
from celery import Celery


celery_redis = Celery('simple_worker', broker='redis://redis:6379/0', backend='redis://redis:6379/0')


class ReorderArgView(MethodView):
    def get(self, task_id):
        result = celery_redis.AsyncResult(task_id).result
        return "Result of the Task from Redis" + str(result)


class ReorderView(MethodView):
    def get(self):
        app.logger.info("Invoking Method ")
        data = {}
        boards = Board.query.all()
        for board in boards:
            data[board.name] = {'admin': board.admin}
            threads = ThreadTable.query.filter(ThreadTable.board_id==board.board_id).all()
            t = {}
            for thread in threads:
                tid = thread.thread_id
                t[tid] = {}
                t[tid]['content'] = thread.content
                posts = Post.query.filter(Post.thread_id==tid).all()
                p = {}
                for post in posts:
                    pid = post.post_id
                    p[pid] = {}
                    p[pid]['content'] = post.content
                t['post'] = p
            data[board.name]['thread'] = t

        r = celery_redis.send_task('tasks.reorder_task', kwargs={'data': data})
        resp = make_response(redirect(url_for('reorder-arg', task_id=r.id)))
        return resp


class LoginView(MethodView):
    def get(self):
        token = request.cookies.get('acc_token')
        if token and token != 'NULL':
            token = decode_auth_token(token)
            if token:
                accepted = Invite.query.filter(Invite.moderator == token[0]).filter(Invite.accepted==1).all()
                new_invite = Invite.query.filter(Invite.moderator == token[0]).filter(Invite.accepted==0).first()
                moderators = []
                if new_invite:
                    moderator = {'board_id': new_invite.board_id, 'admin': new_invite.admin,
                                 'created_on': new_invite.created_on, 'accepted': new_invite.accepted}
                    moderators.append(moderator)
                if accepted:
                    for accept in accepted:
                        moderator = {'board_id': accept.board_id, 'admin': accept.admin,
                                 'created_on': accept.created_on, 'accepted': accept.accepted}
                        moderators.append(moderator)

                data = {'boards': get_boards_data(), 'role': token[1], 'email': token[0], 'moderator': moderators}
                return render_template('logged_in.html', data=data)
            else:
                return render_template('home.html', data={'status_code': 128, 'message': 'Invalid Token'})
        return render_template('home.html', data={'status_code': 0})

    def post(self):
        email = request.form.get('email')
        pass_hash = request.form.get('pass_hash')
        login_token = get_login_token(email, pass_hash)
        data = {'message': login_token['message'], 'status_code': login_token['status_code']}
        if login_token['status_code'] == 0:
            resp = make_response(redirect('/'))
            resp.set_cookie('acc_token', login_token['token'])
            return resp
            # resp = make_response(render_template('logged_in.html', data=data))
            # resp.set_cookie('acc_token', login_token['token'])
            # return resp
        else:
            return render_template('home.html', data=data)


class SignUpView(MethodView):
    def get(self):
        return render_template('signup.html', data={'status_code': 0})

    def post(self):
        email = request.form.get('email')
        pass_hash = request.form.get('pass_hash')
        signup_token = get_signup_token(email, pass_hash)
        if signup_token['status_code'] == 0:
            resp = make_response(redirect('/'))
            resp.set_cookie('acc_token', signup_token['token'])
            return resp
        else:
            data = {'message': signup_token['message'], 'status_code': signup_token['status_code']}
            return render_template('signup.html', data=data)


class LogOutView(MethodView):
    def get(self):
        resp = make_response(redirect('/'))
        resp.set_cookie('acc_token', 'NULL')
        return resp


class BoardView(MethodView):
    def get(self, board_id):
        board = Board.query.filter(Board.board_id==board_id).first()
        if board:
            name = board.name
            threads = ThreadTable.query.filter(ThreadTable.board_id==board_id).all()
            thrds = []
            for thread in threads:
                post = Post.query.filter(Post.thread_id==thread.thread_id).all()
                thrd = {'board_id': thread.board_id, 'admin': thread.admin, 'content': thread.content,
                        'created_on': thread.created_on, 'post': post, 'thread_id': thread.thread_id}
                thrds.append(thrd)
            token = request.cookies.get('acc_token')
            if token and token != 'NULL':
                token = decode_auth_token(token)
                if token:
                    email, role = token
                    invites = Invite.query.filter(Invite.board_id == board_id).filter(Invite.accepted == 1).all()
                    modr = None
                    for invite in invites:
                        if invite.moderator == email:
                            modr = True
                    return render_template('board-view.html', data={'status_code': 0, 'board_id': board_id,
                            'board_name': name, 'thread': thrds, 'role': role, 'email': email, 'moderator': modr})
                else:
                    return render_template('home.html', data={'status_code': 128, 'message': 'Invalid Token'})
        else:
            return render_template('home.html', data={'status_code': 256, 'message': 'Invalid Board id'})


class BoardAddView(MethodView):
    def get(self):
        return render_template('board-add.html', data={'status_code': 0})

    def post(self):
        board_name = request.form.get('board')
        token = request.cookies.get('acc_token')
        admin, role = decode_auth_token(token)
        if role == 5:
            return render_template('board-add.html', data={'status_code': 128, 'message': 'Guest cant add board'})
        try:
            new_board = Board(board_name, admin)
            db.session.add(new_board)
            db.session.commit()
            message = 'Board {} added'.format(board_name)
            return render_template('board-add.html', data={'status_code': 0, 'message': message})
        except Exception as e:
            # if 'duplicate key' in e:
            #     return render_template('board-add.html', data={'status_code': 256, 'message': 'Already exists'})
            return render_template('board-add.html', data={'status_code': 256, 'message': e})


class ThreadAddView(MethodView):
    def get(self, board_id):
        return render_template('thread-add.html', data={'status_code': 0, 'board_id': board_id})


class ThreadAddPostView(MethodView):
    def post(self):
        board_id = request.form.get('board_id')
        content = request.form.get('thread_content')
        board = Board.query.filter(Board.board_id==board_id).first()
        board_name = board.name
        token = request.cookies.get('acc_token')
        admin, role = decode_auth_token(token)
        invites = Invite.query.filter(Invite.moderator==admin).filter(Invite.accepted==1).\
            filter(Invite.board_id==board_id).all()
        modr = False
        for invite in invites:
            if invite.moderator == admin:
                modr = True
                break
        if role == 5 and modr is False:
            return render_template('thread-add.html', data={'status_code': 128, 'message': 'Guest cant add thread'})
        try:
            new_thread = ThreadTable(board_id, admin, content)
            db.session.add(new_thread)
            db.session.commit()
            resp = make_response(redirect(url_for('board-view', board_id=board_id)))
            return resp
        except Exception as e:
            return render_template('board-view.html', data={'status_code': 256, 'message': e})


class PostAddView(MethodView):
    def get(self, board_id, thread_id):
        return render_template('post-add.html', data={'status_code': 0, 'board_id': board_id, 'thread_id': thread_id})


class PostAddPostView(MethodView):
    def post(self):
        board_id = request.form.get('board_id')
        thread_id = request.form.get('thread_id')
        content = request.form.get('post_content')
        board = Board.query.filter(Board.board_id==board_id).first()
        board_name = board.name
        token = request.cookies.get('acc_token')
        admin, role = decode_auth_token(token)
        invites = Invite.query.filter(Invite.moderator==admin).filter(Invite.accepted==1).\
            filter(Invite.board_id==board_id).all()
        modr = False
        for invite in invites:
            if invite.moderator == admin:
                modr = True
                break
        if role == 5 and modr is False:
            return render_template('post-add.html', data={'status_code': 128, 'message': 'Guest cant add thread'})
        try:
            new_post = Post(thread_id, board_id, admin, content)
            db.session.add(new_post)
            db.session.commit()
            resp = make_response(redirect(url_for('board-view', board_id=board_id)))
            return resp
        except Exception as e:
            return render_template('board-view.html', data={'status_code': 256, 'message': e})


class BoardEditView(MethodView):
    def get(self, board_id):
        board = Board.query.filter(Board.board_id==board_id).first()
        name = board.name
        return render_template('board-edit.html', data={'status_code': 0, 'board_id': board_id, 'board_name': name})


class BoardEditPostView(MethodView):
    def post(self):
        board_id = request.form.get('board_id')
        board_name = request.form.get('board_name')
        token = request.cookies.get('acc_token')
        admin, role = decode_auth_token(token)
        invites = Invite.query.filter(Invite.moderator==admin).filter(Invite.accepted==1).\
            filter(Invite.board_id==board_id).all()
        modr = False
        for invite in invites:
            if invite.moderator == admin:
                modr = True
                break
        if role == 5 and modr is False:
            return render_template('board-edit.html', data={'status_code': 128, 'message': 'Guest cant edit board'})
        try:
            board = Board.query.filter(Board.board_id == board_id).first()
            board.name = board_name
            db.session.commit()

            message = 'Board {} updated to {}'.format(board_id, board_name)
            resp = make_response(redirect('/'))
            return resp
        except Exception as e:
            # if 'duplicate key' in e:
            #     return render_template('board-add.html', data={'status_code': 256, 'message': 'Already exists'})
            return render_template('board-edit.html', data={'status_code': 256, 'message': e})


class ThreadEditView(MethodView):
    def get(self, board_id, thread_id):
        thread1 = ThreadTable.query.filter(ThreadTable.thread_id==thread_id).first()
        content = thread1.content
        return render_template('thread-edit.html', data={'status_code': 0, 'board_id': board_id,
                                    'thread_id': thread_id, 'content': content})


class ThreadEditPostView(MethodView):
    def post(self):
        content = request.form.get('content')
        board_id = request.form.get('board_id')
        thread_id = request.form.get('thread_id')
        token = request.cookies.get('acc_token')
        admin, role = decode_auth_token(token)
        invites = Invite.query.filter(Invite.moderator==admin).filter(Invite.accepted==1).\
            filter(Invite.board_id==board_id).all()
        modr = False
        for invite in invites:
            if invite.moderator == admin:
                modr = True
                break
        if role == 5 and modr is False:
            return render_template('thread-edit.html', data={'status_code': 128, 'message': 'Guest cant edit board'})
        try:
            thread1 = ThreadTable.query.filter(ThreadTable.thread_id == thread_id).first()
            thread1.content = content
            db.session.commit()
            message = 'Thread {} updated'.format(thread_id)
            resp = make_response(redirect(url_for('board-view', board_id=board_id)))
            return resp
        except Exception as e:
            return render_template('thread-edit.html', data={'status_code': 256, 'message': e})


class PostEditView(MethodView):
    def get(self, board_id, post_id):
        post = Post.query.filter(Post.post_id==post_id).first()
        content = post.content
        return render_template('post-edit.html', data={'status_code': 0, 'board_id': board_id,
                                    'post_id': post_id, 'content': content})


class PostEditPostView(MethodView):
    def post(self):
        content = request.form.get('content')
        board_id = request.form.get('board_id')
        post_id = request.form.get('post_id')
        token = request.cookies.get('acc_token')
        admin, role = decode_auth_token(token)
        invites = Invite.query.filter(Invite.moderator==admin).filter(Invite.accepted==1).\
            filter(Invite.board_id==board_id).all()
        modr = False
        for invite in invites:
            if invite.moderator == admin:
                modr = True
                break
        if role == 5 and modr is False:
            return render_template('post-edit.html', data={'status_code': 128, 'message': 'Guest cant edit board'})
        try:
            post = Post.query.filter(Post.post_id == post_id).first()
            post.content = content
            db.session.commit()
            resp = make_response(redirect(url_for('board-view', board_id=board_id)))
            return resp
        except Exception as e:
            return render_template('post-edit.html', data={'status_code': 256, 'message': e})


class BoardDeleteView(MethodView):
    def get(self, board_id):
        Board.query.filter(Board.board_id == board_id).delete()
        ThreadTable.query.filter(ThreadTable.board_id == board_id).delete()
        Post.query.filter(Post.board_id == board_id).delete()
        db.session.commit()
        resp = make_response(redirect('/'))
        return resp


class ThreadDeleteView(MethodView):
    def get(self, board_id, thread_id):
        ThreadTable.query.filter(ThreadTable.thread_id == thread_id).delete()
        Post.query.filter(Post.thread_id == thread_id).delete()
        db.session.commit()
        resp = make_response(redirect(url_for('board-view', board_id=board_id)))
        return resp


class PostDeleteView(MethodView):
    def get(self, board_id, post_id):
        Post.query.filter(Post.post_id == post_id).delete()
        db.session.commit()
        resp = make_response(redirect(url_for('board-view', board_id=board_id)))
        return resp


class GravatarView(MethodView):
    def get(self):
        try:
            token = request.cookies.get('acc_token')
            token = decode_auth_token(token)
            if token:
                email, role = token
                user = User.query.filter(User.email == email).first()
                user_id = user.user_id
                url = "https://i.pravatar.cc/300?img={}".format(user_id)
                gravatar_url = "https://www.gravatar.com/avatar/" + hashlib.md5(email.lower().encode('utf-8')).hexdigest() + "?"
                gravatar_url += urllib.parse.urlencode({'d': url, 's': str(300)})
                return redirect(gravatar_url)
            else:
                return "NULL"
        except Exception as e:
            return "NULL"


class InviteView(MethodView):
    def post(self):
        try:
            token = request.cookies.get('acc_token')
            token = decode_auth_token(token)
            if token:
                email, role = token
            option = request.form.get('flexRadioDefault')
            board_id = request.form.get('board_id')
            invite = Invite.query.filter(Invite.moderator == email).filter(Invite.accepted == 0).\
                filter(Invite.board_id==board_id).first()
            if option == 'yes':
                invite.accepted = 1
            else:
                Invite.query.filter(Invite.moderator == email).filter(Invite.accepted == 0). \
                    filter(Invite.board_id == board_id).delete()
            db.session.commit()
            resp = make_response(redirect('/'))
            return resp
        except Exception as e:
            resp = make_response(redirect('/'))
            return resp


class SendInviteView(MethodView):
    def get(self, board_id):
        users = User.query.all()
        return render_template('send-invite.html', data={'board_id': board_id, 'users': users})


class SendInvitePostView(MethodView):
    def post(self):
        try:
            token = request.cookies.get('acc_token')
            token = decode_auth_token(token)
            if token:
                email, role = token
            if role <= 2:
                board_id = request.form.get('board_id')
                moderator = request.form.get('moderator')
                user = User.query.filter(User.email==moderator).first()
                if user:
                    new_invite = Invite(board_id, moderator, email)
                    db.session.add(new_invite)
                    db.session.commit()
            resp = make_response(redirect(url_for('board-view', board_id=board_id)))
            return resp
        except Exception as e:
            resp = make_response(redirect(url_for('board-view', board_id=board_id)))
            return resp

