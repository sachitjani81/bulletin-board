from flask_restful import Resource, Api, marshal_with
from views import *
from models import *

api = Api(app)


@api.resource('/users')
class UserListAPI(Resource):
    @marshal_with(user_fields)
    def get(self):
        return User.query.all()

    @marshal_with(user_fields)
    def post(self):
        new_user = User(request.json['email'], request.json['pass_hash'])
        db.session.add(new_user)
        db.session.commit()
        return new_user


@api.resource('/signup-api')
class SignupAPI(Resource):
    @marshal_with(token_fields)
    def post(self):
        try:
            email = request.json['email']
            pass_hash = request.json['pass_hash']
            return get_signup_token(email, pass_hash)
        except Exception as e:
            raise Exception(e)


@api.resource('/login-api')
class LoginAPI(Resource):
    @marshal_with(token_fields)
    def post(self):
        try:
            email = request.json['email']
            pass_hash = request.json['pass_hash']
            return get_login_token(email, pass_hash)
        except Exception as e:
            raise Exception(e)


app.add_url_rule('/', view_func=LoginView.as_view('login'))
app.add_url_rule('/signup', view_func=SignUpView.as_view('signup'))
app.add_url_rule('/board/add', view_func=BoardAddView.as_view('board-add'))
app.add_url_rule('/board/edit/<int:board_id>', view_func=BoardEditView.as_view('board-edit'))
app.add_url_rule('/board/edit', view_func=BoardEditPostView.as_view('board-edit-post'))
app.add_url_rule('/board/delete/<int:board_id>', view_func=BoardDeleteView.as_view('board-delete'))
app.add_url_rule('/board/view/<int:board_id>', view_func=BoardView.as_view('board-view'))
app.add_url_rule('/thread/add/<int:board_id>', view_func=ThreadAddView.as_view('thread-add'))
app.add_url_rule('/thread/add', view_func=ThreadAddPostView.as_view('thread-add-post'))
app.add_url_rule('/thread/edit/<int:board_id>/<int:thread_id>', view_func=ThreadEditView.as_view('thread-edit'))
app.add_url_rule('/thread/edit', view_func=ThreadEditPostView.as_view('thread-edit-post'))
app.add_url_rule('/thread/delete/<int:board_id>/<int:thread_id>', view_func=ThreadDeleteView.as_view('thread-delete'))
app.add_url_rule('/post/edit/<int:board_id>/<int:post_id>', view_func=PostEditView.as_view('post-edit'))
app.add_url_rule('/post/edit', view_func=PostEditPostView.as_view('post-edit-post'))
app.add_url_rule('/post/delete/<int:board_id>/<int:post_id>', view_func=PostDeleteView.as_view('post-delete'))
app.add_url_rule('/post/add/<int:board_id>/<int:thread_id>', view_func=PostAddView.as_view('post-add'))
app.add_url_rule('/post/add', view_func=PostAddPostView.as_view('post-add-post'))
app.add_url_rule('/logout', view_func=LogOutView.as_view('logout'))
app.add_url_rule('/gravatar', view_func=GravatarView.as_view('gravatar'))
app.add_url_rule('/invite', view_func=InviteView.as_view('invite'))
app.add_url_rule('/send-invite', view_func=SendInvitePostView.as_view('send-invite-post'))
app.add_url_rule('/send-invite/<int:board_id>', view_func=SendInviteView.as_view('send-invite'))
app.add_url_rule('/reorder', view_func=ReorderView.as_view('reorder'))
app.add_url_rule('/reorder/<string:task_id>', view_func=ReorderArgView.as_view('reorder-arg'))


if __name__ == '__main__':
    app.run(debug=True)
