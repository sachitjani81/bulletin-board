from flask import Flask
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from flask_restful import fields
import datetime
import jwt
import os

load_dotenv()
app = Flask(__name__)
bcrypt = Bcrypt(app)

app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')  # Generated using secrets.token_hex(16)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://{}:{}@db:5432/bulletin_board'.format(
    os.getenv('DB_USER'), os.getenv('DB_PASS')
)
db = SQLAlchemy(app)


def encode_auth_token(email, role):
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=500),
            'iat': datetime.datetime.utcnow(),
            'sub': (email, role)
        }
        return jwt.encode(payload, app.config.get('SECRET_KEY'), algorithm='HS256')
    except Exception as e:
        return e


def decode_auth_token(auth_token):
    try:
        payload = jwt.decode(auth_token, app.config.get('SECRET_KEY'), algorithms='HS256')
        return payload['sub']
    except jwt.ExpiredSignatureError:
        return 'Signature Expired!! Login Again!!'
    except jwt.InvalidTokenError:
        return 'Invalid Token!! Login Again!!'


class User(db.Model):
    __tablename__ = 'users'
    user_id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, nullable=False)
    pass_hash = db.Column(db.String, nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)
    role = db.Column(db.Integer, nullable=False)

    def __init__(self, email, pass_hash, role=5):
        self.email = email
        self.pass_hash = bcrypt.generate_password_hash(pass_hash).decode('UTF-8')
        self.role = role
        self.created_on = datetime.datetime.now(datetime.timezone.utc)


class Board(db.Model):
    __tablename__ = 'board'
    board_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    admin = db.Column(db.String, nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, name, admin):
        self.name = name
        self.admin = admin
        self.created_on = datetime.datetime.now(datetime.timezone.utc)


class Invite(db.Model):
    __tablename__ = 'invite'
    iid = db.Column(db.Integer, primary_key=True)
    board_id = db.Column(db.Integer, nullable=False)
    moderator = db.Column(db.String, nullable=False)
    admin = db.Column(db.String, nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)
    accepted = db.Column(db.Integer)

    def __init__(self, board_id, moderator, admin):
        self.board_id = board_id
        self.moderator = moderator
        self.admin = admin
        self.created_on = datetime.datetime.now(datetime.timezone.utc)
        self.accepted = 0


class ThreadTable(db.Model):
    __tablename__ = 'thread_table'
    thread_id = db.Column(db.Integer, primary_key=True)
    board_id = db.Column(db.Integer, nullable=False)
    admin = db.Column(db.String, nullable=False)
    content = db.Column(db.String, nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, board_id, admin, content):
        self.board_id = board_id
        self.admin = admin
        self.content = content
        self.created_on = datetime.datetime.now(datetime.timezone.utc)


class Post(db.Model):
    __tablename__ = 'post'
    post_id = db.Column(db.Integer, primary_key=True)
    thread_id = db.Column(db.Integer, nullable=False)
    board_id = db.Column(db.Integer, nullable=False)
    admin = db.Column(db.String, nullable=False)
    content = db.Column(db.String, nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, thread_id, board_id, admin, content):
        self.thread_id = thread_id
        self.board_id = board_id
        self.admin = admin
        self.content = content
        self.created_on = datetime.datetime.now(datetime.timezone.utc)


board_fields = {
    'name': fields.String,
    'admin': fields.String,
    'created_on': fields.DateTime,
}

user_fields = {
    'user_id': fields.Integer,
    'email': fields.String,
    'created_on': fields.DateTime,
    'role': fields.Integer
}

token_fields = {
    'token': fields.String,
    'message': fields.String,
    'status_code': fields.Integer
}


def get_boards_data():
    try:
        boards = Board.query.all()
        return boards
    except Exception as e:
        raise e


def get_login_token(email, pass_hash):
    try:
        db_entries = db.session.query(User.pass_hash, User.role).filter_by(email=email).first()
        if db_entries is None:
            return {'token': 'NULL', 'message': 'Email Address not found!!', 'status_code': 128}
        elif bcrypt.check_password_hash(db_entries[0], pass_hash):
            return {'token': encode_auth_token(email, db_entries[1]), 'message': 'Success', 'status_code': 0}
        else:
            return {'token': 'NULL', 'message': 'Invalid password!!', 'status_code': 128}
    except ValueError as v:
        return {'token': 'NULL', 'message': v, 'status_code': 256}
    except Exception as e:
        return {'token': 'NULL', 'message': e, 'status_code': 256}


def get_signup_token(email, pass_hash):
    try:
        already_exists = db.session.query(User.user_id).filter_by(email=email).first()
        if already_exists is not None:
            return {'token': 'NULL', 'message': 'Email already in use!! Try Login', 'status_code': 128}
        else:
            total_users = User.query.count()
            if total_users == 0:
                role = 1
                new_user = User(email, pass_hash, role=role)  # 1st user will be Super user
            else:
                role = 5
                new_user = User(email, pass_hash, role=role)  # Default Guest user

            db.session.add(new_user)
            db.session.commit()
            return {'token': encode_auth_token(email, role), 'message': 'Success', 'status_code': 0}
    except Exception as e:
        return {'token': 'NULL', 'message': e, 'status_code': 256}
