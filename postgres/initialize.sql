
CREATE TABLE IF NOT EXISTS users (
  user_id SERIAL,
  email VARCHAR(255) UNIQUE NOT NULL,
  pass_hash VARCHAR(255) NOT NULL,
  created_on TIMESTAMP NOT NULL,
  role INT NOT NULL,
  PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS board (
  board_id SERIAL,
  name VARCHAR(255) UNIQUE NOT NULL,
  admin VARCHAR(255) NOT NULL,
  created_on TIMESTAMP NOT NULL,
  PRIMARY KEY (board_id)
);

CREATE TABLE IF NOT EXISTS thread_table (
  thread_id SERIAL,
  board_id INT NOT NULL,
  admin VARCHAR(255) NOT NULL,
  content VARCHAR NOT NULL,
  created_on TIMESTAMP NOT NULL,
  PRIMARY KEY (thread_id)
);

CREATE TABLE IF NOT EXISTS post (
  post_id SERIAL,
  thread_id INT NOT NULL,
  board_id INT NOT NULL,
  admin VARCHAR(255) NOT NULL,
  content VARCHAR NOT NULL,
  created_on TIMESTAMP NOT NULL,
  PRIMARY KEY (post_id)
);

CREATE TABLE IF NOT EXISTS invite (
    iid SERIAL,
    board_id INT NOT NULL,
    moderator VARCHAR(255) NOT NULL,
    admin VARCHAR(255) NOT NULL,
    created_on TIMESTAMP NOT NULL,
    accepted INT,
    PRIMARY KEY (iid)
);
