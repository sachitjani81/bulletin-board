import time
from celery import Celery
from celery.utils.log import get_task_logger
from itertools import groupby

logger = get_task_logger(__name__)

app = Celery('tasks', broker='redis://redis:6379/0', backend='redis://redis:6379/0')


@app.task()
def reorder_task(data):
    logger.info('Got Request - Starting work ')
    new_data = []
    board_names = list(data.keys())
    board_names.sort()
    grouped_results = [list(i) for j, i in groupby(board_names, lambda a: a.split(' ')[0])]
    for results in grouped_results:
        for result in results:
            new_data.append((result, data[result]))
    logger.info('Work Finished ')
    return new_data
